
## Hi there, I'm I Wayan Alston.👋            
                            
Aka Batata

👀 I'm interested in mobile development, currently I'm still in android native development

🎯 I want to Contribute more to an open-source world

🔥 Besides that I also like to challenge myself in [HackerRank](https://www.hackerrank.com/alstonargodi01)

🤝 Want to know more ? Connect with me on my [Twitter](https://twitter.com/ArgodiI)

📝 Sometimes I write a short story when I'm bored, I'm pouring my boredom into  [Medium](https://medium.com/@alstonargodi)



## Programming Language 

<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/kotlin/kotlin-original.svg" />&nbsp;&nbsp;&nbsp;&nbsp;<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" />&nbsp;&nbsp;<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg" />&nbsp;&nbsp;<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/java/java-original.svg" />&nbsp;&nbsp;<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-original.svg" />
          
## Tools 

<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/androidstudio/androidstudio-original.svg"/>&nbsp;&nbsp;<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/intellij/intellij-original.svg" />&nbsp;&nbsp;<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" />&nbsp;&nbsp;<img height="50" width="50" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jupyter/jupyter-original-wordmark.svg" />
                                                                                                                         
          
<br />

---


![](https://komarev.com/ghpvc/?username=rogerboto&color=blue)

[twitter]: https://twitter.com/ArgodiI

